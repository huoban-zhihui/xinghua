<?php
/*
 * @Author: ZhaohangYang <yangzhaohang@comsenz-service.com>
 * @Date: 2021-06-23 16:58:47
 * @Description: 伙伴智慧大客户研发部
 */
namespace App\ModesXinghua;

use App\ModesXinghua\XinghuaBasic;

class XinghuaDocking extends XinghuaBasic
{
    public function dockingSync($params)
    {
        $url  = '/services/CurrService?wsdl';
        $body = $this->getDockingXml($params);
        // $body = $this->getDockingArray($params);

        $response = $this->sendRequest($url, $body);
        return $response;
    }

    public function getDockingArray($params)
    {
        $time = date('Y-m-d H:i:s', time());
        $body = [
            'Request' => [
                'Header' => [
                    'SourceSystemCode' => 'HRS',
                    'SourceSystemName' => '出入库系统',
                    'MessageID'        => '10008' . $params['item_id'],
                    'MessageCode'      => 'S008',
                    'MessagegTime'     => $time,
                ],
                'Body'   => [
                    'Empid'              => '10008',
                    'Delivery_time'      => $params['Delivery_time'],
                    'Delivery_number'    => $params['Delivery_number'],
                    'Amount'             => $params['Amount'],
                    'Purchase_code'      => $params['Purchase_code'],
                    'Price'              => $params['Price'],
                    'Certificate_number' => $params['Certificate_number'],
                    'Winning_code'       => $params['Winning_code'],
                    'Trade_name'         => $params['Trade_name'],
                    'Specifications'     => $params['Specifications'],
                    'unit'               => $params['unit'],
                    'status'             => $params['status'],
                    'Brand'              => $params['Brand'],
                    'Create_time'        => $params['Create_time'],
                ],
            ],
        ];

        return $body;
    }

    public function getDockingXml($params)
    {
        $time = date('Y-m-d H:i:s', time());
        $body = <<<EOT
<Request>
<Header>
<SourceSystemCode>HRS</SourceSystemCode>
<SourceSystemName>出入库系统</SourceSystemName>
<MessageID>10008{$params['item_id']}</MessageID>
<MessageCode>S008</MessageCode>
<MessagegTime>$time</MessagegTime>
</Header>
<Body>
<Empid>10008</Empid>
<Delivery_time>{$params['Delivery_time']}</Delivery_time>
<Delivery_number>{$params['Delivery_number']}</Delivery_number>
<Amount>{$params['Amount']}</Amount>
<Purchase_code>{$params['Purchase_code']}</Purchase_code>
<Price>{$params['Price']}</Price>
<Certificate_number>{$params['Certificate_number']}</Certificate_number>
<Winning_code>{$params['Winning_code']}</Winning_code>
<Trade_name>{$params['Trade_name']}</Trade_name>
<Specifications>{$params['Specifications']}</Specifications>
<unit>{$params['unit']}</unit>
<status>{$params['status']}</status>
<Brand>{$params['Brand']}</Brand>
<Create_time>{$params['Create_time']}</Create_time>
</Body>
</Request>
EOT;

        return $body;

    }

}
