<?php

namespace App\Http\Controllers;

use App\ModesXinghua\XinghuaDocking;
use Illuminate\Http\Request;

class IndexController extends Controller
{
    public function index(Request $request)
    {
        try {
            $params = $request->all();

            $xinghua_docking = new XinghuaDocking();
            $response        = $xinghua_docking->dockingSync($params);

            return Response()->json(['code' => '0', 'message' => '请求无异常', 'success' => true, 'data' => $response]);
        } catch (\Throwable $th) {
            return Response()->json(['message' => $th->getMessage(), 'file' => $th->getFile(), 'line' => $th->getLine()], 500);
        }

    }
}
