<?php
/*
 * @Author: SanQian
 * @Date: 2021-08-10 09:24:48
 * @LastEditTime: 2021-10-21 11:43:58
 * @LastEditors: SanQian
 * @Description:
 * @FilePath: /xinghuayiyuan/app/Http/Controllers/TestController.php
 *
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TestController extends Controller
{
    public function test(Request $request)
    {

        $response_obj = simplexml_load_string($request->getContent(), "SimpleXMLElement", LIBXML_NOCDATA);
        print_r(json_encode($response_obj));die();
        print_r(json_decode(json_encode($response_obj), true));die();

        $xml = new \SimpleXMLElement('<?xml version="1.0" encoding="UTF-8" ?><websites></websites>');

        $website = $xml->addChild('website');
        $website->addChild('url', 'https://learnku.com');
        $website->addChild('desc', 'LearnKu 是终身学习者的编程知识社区');
        $content = $xml->asXML();

        return response($content)->header('Content-Type', 'text/xml');

    }
}
