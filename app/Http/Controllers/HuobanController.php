<?php
namespace App\Http\Controllers;

use Huoban\Huoban;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Cookie;
use Laravel\Lumen\Routing\Controller as BaseController;

class HuobanController extends BaseController
{
    /**
     * 当前访问的应用id
     *
     * @var [string]
     */
    public $appId;
    /**
     * 当前操作人对应应用的ticket
     *
     * @var [string]
     */
    public $ticket;
    /**
     * 当前访问的表格id
     *
     * @var [string]
     */
    public $tableId;

    /**
     * 企业伙伴操作对象
     *
     * @var [Huoban\Huoban]
     */
    public $_huoban;
    /**
     * 个人伙伴操作对象
     *
     * @var [Huoban\Huoban]
     */
    public $_userHuoban;

    public $request;

    public $response;

    public function __construct(Request $request, Response $response)
    {
        ini_set('max_execution_time', 30);
        $this->request  = $request;
        $this->response = $response;

        $this->setTableTicket($request);
        $this->setAppId($request);
        $this->setTableId($request);

        $config            = config('huoban.huoban_pass');
        $this->_huoban     = new Huoban($config);
        $this->_userHuoban = new Huoban(['ticket' => $this->ticket] + $config);
    }
    /**
     * cookie 存储表格应用ticket信息
     *
     * @param [type] $request
     * @return void
     */
    public function setTableTicket($request)
    {
        $this->ticket = $request->input('ticket', null) ?: Cookie::get('ticket');
        $this->response->withCookie(new \Symfony\Component\HttpFoundation\Cookie('ticket', $this->ticket, time() + 60 * 60 * 24));
    }
    /**
     * cookie 存储表格应用app_id信息
     *
     * @param [type] $request
     * @return void
     */
    public function setAppId($request)
    {
        $this->appId = $request->input('app_id', null) ?: Cookie::get('app_id');
        $this->response->withCookie(new \Symfony\Component\HttpFoundation\Cookie('app_id', $this->appId, time() + 60 * 60 * 24));
    }
    /**
     * 存储表格应用table_id信息
     *
     * @param [type] $request
     * @return void
     */
    public function setTableId($request)
    {
        $this->tableId = $request->input('table_id', null) ?: Cookie::get('table_id');
        $this->response->withCookie(new \Symfony\Component\HttpFoundation\Cookie('table_id', $this->tableId, time() + 60 * 60 * 24));
    }
    /**
     * 获取当前操作人id
     *
     * @return void
     */
    public function getUserId()
    {
        if (isset($this->userId) && $this->userId) {
            return $this->userId;
        }

        $data         = $this->_userHuoban->_ticket->parse();
        $this->userId = $data['user']['user_id'];

        return $this->userId;
    }

    public function getTable(Request $request)
    {
        $table_id = $request->input('tableId');
        $table    = $this->_huoban->_table->get($table_id);

        return response()->json($table);
    }

    public function getTableData(Request $request)
    {
        $body       = $request->input('body');
        $table_id   = $request->input('tableId');
        $table_data = $this->_huoban->_item->find($table_id, $body);

        return response()->json($table_data);
    }
}
