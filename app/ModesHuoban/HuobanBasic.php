<?php
/*
 * @Author: ZhaohangYang <yangzhaohang@comsenz-service.com>
 * @Date: 2021-06-23 16:58:47
 * @Description: 伙伴智慧大客户研发部
 */
namespace App\ModesHuoban;

use Illuminate\Support\Facades\Log;

class HuobanBasic
{
    public $_huoban;
    public $tableAlias;

    public function __construct($_huoban)
    {
        $this->_huoban = $_huoban;
    }
    /**
     * 效验伙伴请求返回结果
     *
     * @param [type] $response
     * @param string $location
     * @return void
     */
    public function verifyHuobanResponse($response, $location = '', $type = 'throw')
    {
        if (isset($response['code'])) {
            $message = $response['message'] ?? '未知错误信息';
            if ('log' == $type) {
                Log::error($location . $message);
            } else {
                throw new \Exception($location . $message, 10001);
            }
        }
    }

    public function collectError($message, $item_id = null)
    {
        if ($item_id) {
            $body = $this->getErrorBody($message);
            $this->_huoban->_item->update($item_id, $body);
        }

    }
    public function getErrorBody($message)
    {
        $body = [
            'fields' => [
                'F::' . $this->tableAlias . '.app_error_message' => $message,
            ],
        ];
        return $body;
    }

}
